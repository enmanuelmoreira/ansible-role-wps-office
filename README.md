# Ansible Role: WPS Office

This role installs [WPS Office](https://wps.com/) binary on any supported host.

## Requirements

None

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    wps_version: 11.1.0
    wps_release: 11702
    wps_package_name: wps-office

This role always installs the desired version. See [available WPS Office releases](https://wps.com).

    wps_version: 4.22.0

The version to the WPS Office application.

    wps_release: 11702

The release number of the package.

    wps_package_name: wps-office

The name of the package installed in the system (if there need to be updated it).

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: wps-office

## License

MIT / BSD
